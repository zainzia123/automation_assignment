# Senior QA Engineer Pre-assessment

Pre-Assessment Project


# Automation Assignment 

I used the following folder structure in my test environmnet

- TestCases: In this folder there is a excel which has a Excel file in which all the test cases are define.
- Screenshots: In this folder we have screenshot against every test case we define in the excel sheet. In these images we implement image processing on it.
- Exceptions: In any of the region if you got any exception it will be logged in this folder to help out the logging in the automation suit.
- OutputResults: After every execution of the test cases it will create a test results output in the form of excel in this folder.


# Pacakges

I use following libraries in C# Unit Test Project
  - IronOcr
  - OpenQA.Selenium
  - Microsoft.Office.Interop.Excel
  - System.IO
  - WindowsInput

If you find any package unresolved in the Dependencies folder you can easily resolved by right-click on package and then click on "Resolve Depedencies". 

# Requirements

  - You need to update the chromedriver.exe with your version of chrome. You can download it from the link (https://chromedriver.chromium.org/downloads)
  - You need to install Microsoft Excel 2013+ just because I used the updated library to read excel sheet.
 
# Usage

This project is separated into different regions of the code.Each of these module is very Important for the clean approach of the development.
 
 - Fields: to define all the common variables in the test environment.
 - TestIntialize: it will intialize all the important thing you need to run the automation suit. It will load the chrome with driver and maximize its screen. it will also help us to screenshot of the output canvas by element Id.
 - Execution: it will run the test cases and define its output in the output directory of the folder.
 - Private Methods: Having Private methods to help out the Separate Principle Concept in the development.
 - TestCleanup: It will dispose all the objects we used to avoid memory problems.
  
   

