﻿using IronOcr;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Threading;
using WindowsInput;
using WindowsInput.Native;
using Excel = Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Linq;

namespace Automation.Assignment
{
    [TestClass]
    public class TestEnvironment
    {
        #region fields

        private readonly string _calculatorWebURL = "https://www.online-calculator.com/full-screen-calculator/";
        private readonly string _testCasesPath = Directory.GetCurrentDirectory() + @"\TestCases\TestCases.xlsx";
        private ChromeDriver _driver = new ChromeDriver();
        private InputSimulator _inputSimulator = new InputSimulator();
        private DataTable _dataTable;
        private AutoOcr _ocr = new AutoOcr() { ReadBarCodes = false,};

        #endregion


        #region Initialize Test Environment

        /// <summary>
        /// It will Intialize the complete suit
        /// </summary>
        [TestInitialize]
        public void TestEnvironmentIntialize()
        {
            ReadAllTestCases();
            _driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);
            _driver.Manage().Window.Maximize();
            _driver.Navigate().GoToUrl(_calculatorWebURL);
        }

        /// <summary>
        /// It will read All the test Cases Define in the Sheet.
        /// </summary>
        private void ReadAllTestCases()
        {
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook workbook;
            _dataTable = new DataTable();
            try
            {
                //Don't want Excel to display error messageboxes
                excelApp.DisplayAlerts = false;
                //This opens the file
                workbook = excelApp.Workbooks.Open(_testCasesPath,ReadOnly:true);
                //Get the first sheet in the file
                Excel.Worksheet sheet = (Excel.Worksheet)workbook.Sheets.get_Item(1); 

                int lastRow = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Row;
                int lastColumn = sheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Column;
                Excel.Range oRange = sheet.get_Range(sheet.Cells[1, 1], sheet.Cells[lastRow, lastColumn]);//("A1",lastColumnIndex + lastRow.ToString());
                oRange.EntireColumn.AutoFit();

                //for (int i = 0; i < oRange.Columns.Count; i++)
                //{
                //    //var columnName = oRange.Row
                //    _dataTable.Columns.Add("Column" + i.ToString());
                //}
                _dataTable.Columns.Add("TestCaseNumber");
                _dataTable.Columns.Add("Description");
                _dataTable.Columns.Add("Input" );
                _dataTable.Columns.Add("ExpectedResult");
                _dataTable.Columns.Add("ActualResult");

                object[,] cellValues = (object[,])oRange.Value2;
                object[] values = new object[lastColumn];

                for (int i = 2; i <= lastRow; i++)
                {

                    for (int j = 0; j < _dataTable.Columns.Count; j++)
                    {
                        values[j] = cellValues[i, j + 1];
                    }
                    _dataTable.Rows.Add(values);
                }
                workbook.Close(false, Type.Missing, Type.Missing);
            }
            catch (Exception ex)
            {
                File.WriteAllText($"{Directory.GetCurrentDirectory()}/Exceptions/{DateTime.Now.ToString()}.txt", $"{nameof(ReadAllTestCases)}: Exception Message: {ex.Message}");
            }
            finally
            {
                excelApp.Quit();
            }

        }

        #endregion

        #region Execution

        /// <summary>
        /// It will execute test Suit
        /// </summary>
        [TestMethod]
        public void Execute_All_Test_Cases()
        {

            for (int i = 0; i < _dataTable.Rows.Count; i++)
            {
                string testCaseNumber = _dataTable.Rows[i]["TestCaseNumber"].ToString();
                string input = _dataTable.Rows[i]["Input"].ToString();
                string expectedResult = _dataTable.Rows[i]["ExpectedResult"].ToString();
                if (!string.IsNullOrEmpty(testCaseNumber))
                {
                    ExecuteTestCase(testCaseNumber, input, expectedResult);
                    var screenshotPath = TakeScreenshot(testCaseNumber);
                    var valueFromOCR = GetValueFromOCR(screenshotPath);
                    _dataTable.Rows[i]["ActualResult"] = valueFromOCR.ToString();
                }
            }
            ExportResultsToExcel();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// it will run the test Case
        /// </summary>
        /// <param name="testCaseNumber"></param>
        /// <param name="inputValue"></param>
        /// <param name="expectedResult"></param>
        private void ExecuteTestCase(string testCaseNumber, string inputValue, string expectedResult)
        {
            _driver.Navigate().GoToUrl(_calculatorWebURL);
            foreach (var input in inputValue)
            {
                SimulateInput(input);
            }
            SimulateInput('=');
        }

        /// <summary>
        /// It will read the value from Cropped Image
        /// </summary>
        /// <param name="screenshotPath"></param>
        /// <returns></returns>
        private string GetValueFromOCR(string screenshotPath)
        {
            var Result = _ocr.Read(screenshotPath);
            return Result.Text;
        }

        /// <summary>
        /// It will take Screenshot
        /// </summary>
        /// <param name="testCaseNumber"></param>
        /// <returns></returns>
        private string TakeScreenshot(string testCaseNumber)
        {
            string screenshotName = Directory.GetCurrentDirectory() + @"\Screenshots\" + testCaseNumber + ".Jpeg";
            string screenshotNameFullFrame = Directory.GetCurrentDirectory() + @"\Screenshots\" +  testCaseNumber + "_fullframe.jpeg";

            //Delay to simulate like real
            Thread.Sleep(TimeSpan.FromSeconds(2));
            var canvas = _driver.FindElementById("fullframe");
            string logoSRC = canvas.GetAttribute("src");

            Screenshot screenshot = ((ITakesScreenshot)_driver).GetScreenshot();
            screenshot.SaveAsFile(screenshotNameFullFrame, ScreenshotImageFormat.Jpeg);

            Image img = Bitmap.FromFile(screenshotNameFullFrame);
            Rectangle rect = new Rectangle();

            if (canvas != null)
            {
                // Get the Width and Height of the WebElement using
                int width = canvas.Size.Width;
                int height = 100;

                // Get the Location of WebElement in a Point.
                // This will provide X & Y co-ordinates of the WebElement
                Point p = canvas.Location;

                // Create a rectangle using Width, Height and element location
                rect = new Rectangle(p.X, p.Y, width, height);
                Bitmap bmpImage = new Bitmap(img);
                var cropedImag = bmpImage.Clone(rect, bmpImage.PixelFormat);
                cropedImag.Save(screenshotName);

            }
            return screenshotName;
        }

        //It will simulate the test case from the input
        private void SimulateInput(char input)
        {
            //Delay to simulate like real
            Thread.Sleep(TimeSpan.FromSeconds(2));
            switch (input)
            {
                case '0':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD0);
                    break;
                case '1':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD1);
                    break;
                case '2':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD2);
                    break;
                case '3':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD3);
                    break;
                case '4':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD4);
                    break;
                case '5':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD5);
                    break;
                case '6':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD6);
                    break;
                case '7':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD7);
                    break;
                case '8':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD8);
                    break;
                case '9':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.NUMPAD9);
                    break;
                case '-':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.SUBTRACT);
                    break;
                case '+':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.ADD);
                    break;
                case '*':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.DIVIDE);
                    break;
                case '/':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.MULTIPLY);
                    break;
                case '=':
                    _inputSimulator.Keyboard.KeyDown(VirtualKeyCode.RETURN);
                    break;
                case 'c':
                case 'C':
                    _inputSimulator.Keyboard.ModifiedKeyStroke(VirtualKeyCode.SHIFT, VirtualKeyCode.VK_C);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// It will export the Datatable to Excel csv
        /// </summary>
        private void ExportResultsToExcel()
        {
            if (_dataTable.Rows.Count > 0)
            {
                var lines = new List<string>();

                string[] columnNames = _dataTable.Columns.Cast<DataColumn>().
                                                  Select(column => column.ColumnName).
                                                  ToArray();

                var header = string.Join(",", columnNames);
                lines.Add(header);

                var valueLines = _dataTable.AsEnumerable()
                                   .Select(row => string.Join(",", row.ItemArray));
                lines.AddRange(valueLines);

                File.WriteAllLines(Directory.GetCurrentDirectory()+ "/OutputResults/output.csv", lines);
            }
        }

        #endregion

        #region Cleanup test Environment

        /// <summary>
        /// It will Clean all the objects created
        /// </summary>
        [TestCleanup]
        public void Cleanup()
        {
            _dataTable.Dispose();
            _driver.Close();
            _driver.Quit();
            _driver.Dispose();
        }

        #endregion
    }
}
